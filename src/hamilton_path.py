import numpy as np
import pymongo


def connect_database():
    client = pymongo.MongoClient('mongodb://localhost:27017/')
    db = client['matrix']
    return db['vertex']


class BackTracking:
    def __init__(self, collection):
        self.collection = collection
        self.initial_vertex = {}
        self.actual_path = []
        self.saved_path = []
        self.actual_vertex = {}
        self.graph_order = int(collection.count_documents({}))
        self.visited_matrix = np.zeros((self.graph_order, self.graph_order))

    def back_tracking(self):
        check_input = False
        self.initial_vertex = self.collection.find_one({})
        while not check_input:
            index_vertex = int(input("enter starting vertex between 0 and " +
                                     str(self.graph_order - 1) + ": "))
            if 0 <= index_vertex <= self.graph_order - 1:
                check_input = True
                self.initial_vertex = self.collection.find_one({"id_matrix": index_vertex})
        self.actual_path.append(self.initial_vertex["id_matrix"])
        self.actual_vertex = self.initial_vertex

        while len(self.saved_path) < self.graph_order:
            available_neighbors = [val for val in self.actual_vertex["neighbors"]
                                   if not self.visited_matrix[self.actual_vertex["id_matrix"]][val["id"]]
                                   and val["id"] != self.initial_vertex["id_matrix"]
                                   and val["id"] not in self.actual_path
                                   or (self.graph_order == len(self.actual_path)
                                   and val["id"] == self.initial_vertex["id_matrix"])]
            if len(available_neighbors) > 0:
                self.actual_path.append(available_neighbors[0]["id"])
                self.actual_vertex = self.collection.find_one({"id_matrix": available_neighbors[0]["id"]})
            else:
                if len(self.actual_path) == self.graph_order + 1 and self.actual_path[-1] == \
                        self.initial_vertex["id_matrix"]:
                    self.saved_path = self.actual_path
                else:
                    self.visited_matrix[self.actual_path[-2]][self.actual_vertex["id_matrix"]] = 1
                    for i, _ in enumerate(self.visited_matrix[self.actual_vertex["id_matrix"]]):
                        self.visited_matrix[self.actual_vertex["id_matrix"]][i] = 0
                    self.actual_vertex = self.collection.find_one({"id_matrix": self.actual_path[-2]})
                    self.actual_path.pop(-1)
        return self.saved_path


hehe = BackTracking(connect_database())
print(hehe.back_tracking())
