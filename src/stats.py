import matplotlib.pyplot as plt
import numpy as np

path = np.array([7, 0, 1, 2, 4, 3, 5, 21, 8, 9, 12, 10, 11, 15, 14, 13, 16, 17, 18, 19, 20, 22, 6, 24, 23, 7])
iteration = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25])
time = np.array([4.650025367736816,5.999999523162842,7.870981931686401, 10.88599944114685,9.0749990940094,10.59999966621399,15.914762496948242,12.779017686843872,33.4719979763031,27.440999507904053])
nb_sommet = np.array([25,50,75,100,125,150,175,200,225,250])
densite = np.array([50,55,60,65,70,75,80,85,90,95])
time_thickness= np.array([12.493869304656982,9.765828132629395,7.181560754776001,7.937010526657104,6.869084358215332,5.293980598449707,5.562999248504639,4.7719926834106445,4.465254783630371,3.8680055141448975])

plt.title('Corrélation entre le nombre de sommet et le temps d execution')
plt.xlabel('Nombre de sommet')
plt.ylabel('Temps en s')
x = nb_sommet
y = time
plt.plot(x, y)
plt.show()

plt.title('Corrélation entre la densité et le temps d execution')
plt.xlabel('Densité')
plt.ylabel('Temps en s')
x = densite
y = time_thickness
plt.plot(x, y)
plt.show()