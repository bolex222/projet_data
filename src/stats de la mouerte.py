# Import des bibliotéques
import random
import numpy as np
import pymongo
import time
import math
import matplotlib.pyplot as plt


# Initialisation d'une variable globale pour mesure le temps que prend la création du graphe
start = time.time()

# Definition des variables de BDD
bdd_url = "mongodb://localhost:27017/"
base_name = "matrix"
base_collection_name = "vertex"


def matrix_generation(nb_vertices, density):
    global start
    # Démarrage de la mesure du temps
    start = time.time()
    # On créer une matrice binaire aléatoire dont les probabilités sont influencées par la densité renseignée
    matrix = np.random.choice((1, 0), size=(nb_vertices, nb_vertices),
                              p=[(density - 10) / 100, 1 - (density - 10) / 100])
    # On rend la matrice symétrique afin que le graphe ne soit pas orienté
    matrix = np.logical_or(matrix, matrix.T)

    # On retire les arêtes qui bouclent sur un sommet pour simplifier le travail des algorithmes plus tard
    for i in range(nb_vertices):
        matrix[i][i] = 0

        # On liste les arêtes qui ne respectent pas le théorème de Dirac
    not_valid_row = [row_id for row_id, row in enumerate(matrix) if sum(row) < (nb_vertices / 2)]
    while len(not_valid_row) > 0:
        condition_check = False
        row_one = random.choice(not_valid_row)
        # S'il y a plus de deux arêtes qui ne respecte pas le théorème de Dirac, on va essayer de les lier ensemble
        if len(not_valid_row) < 2:
            choice = [row_id for row_id, val in enumerate(matrix[row_one]) if not val and row_id != row_one]
            row_two = random.choice(choice)
        else:  # Quand il ne reste qu'un sommet a compléter on le lie avec un sommet aléatoire du graphe
            temp_not = not_valid_row.copy()
            temp_not.remove(row_one)
            row_two = random.choice(temp_not)
            if matrix[row_one][row_two]:
                choice_row_one = [row_id for row_id, val in enumerate(matrix[row_one]) if not val and row_id != row_one]
                row_two = random.choice(choice_row_one)
            else:
                condition_check = True
        matrix[row_one][row_two] = 1
        matrix[row_two][row_one] = 1
        if sum(matrix[row_one]) >= nb_vertices / 2:
            not_valid_row.remove(row_one)
        if condition_check:
            not_valid_row.remove(row_two)
    return matrix


def weighting_graph(binary_matrix):
    # On crée une matrice d'objet vide basé sur la matrice binaire
    quantity_vertex = len(binary_matrix)
    object_row = [{}] * quantity_vertex
    object_matrix = []
    for i in range(quantity_vertex):
        object_matrix.append(object_row.copy())
    # On parcourt cette matrice d'objet
    for vertex in range(quantity_vertex):
        for vertex2 in range(vertex + 1):
            # Si les deux sommets sont adjacents alors on génère des poids
            if binary_matrix[vertex][vertex2]:
                distance = random.randint(5, 30)
                tm = random.randint(30, 50)
                ta = random.randint(30, 50)
                temp_data = {
                    "distance": distance,
                    "traffic_morning": tm,
                    "traffic_afternoon": ta
                }
                # On remplit la matrice avec les données générées
                object_matrix[vertex][vertex2] = temp_data
                object_matrix[vertex2][vertex] = temp_data
    return object_matrix


def connect_to_bdd():  # Fonction de connexion à la base
    client = pymongo.MongoClient(bdd_url)
    db = client[base_name]
    return db[base_collection_name]


def empty_collection(collection):  # Fonction pour vider la base
    collection.delete_many({})


def fill_bdd(collection, object_matrix):  # Fonction de remplissage de la base
    for id, row in enumerate(
            object_matrix):  # On crée un objet représentant un sommet pour caque sommet de la matrice
        object_vertex = {
            "id_matrix": id,
            "neighbors": []
        }
        # On remplit la liste des voisins basée sur la matrice
        for neighbor_id, neighbor in enumerate(row):
            if neighbor != {}:
                temp_neighbor = neighbor.copy()
                temp_neighbor["id"] = neighbor_id
                object_vertex["neighbors"].append(temp_neighbor)
        # Insertion de l'objet dans la base
        collection.insert_one(object_vertex)


# Génération de la matrice binaire
#Nombre de sommet que nous voulons sur le graphe
nb_vertex = int(input("nombre de sommets du graphe à générer : "))
#Entre 50% et 100%
density = int(input("densité du du graphe entre 50 et 100 % (valeur apprcohé)"))
global_binary_matrix = matrix_generation(nb_vertex,density)
print(global_binary_matrix)
# Pondération du graphe
weighted_graph = weighting_graph(global_binary_matrix)
# Connecter et vider la collection
global_collection = connect_to_bdd()
empty_collection(global_collection)
# Insertion des données dans la collection
fill_bdd(global_collection, weighted_graph)


class BackTracking:
    # On déclare ici les variables qui serviront tout le long de l'algorithme
    def __init__(self, collection):
        self.collection = collection  # La collection de la bdd
        self.initial_vertex = {}  # Variable qui contient le sommet de départ
        self.actual_path = []  # Chemin sur lequel l'algorithme travail
        self.saved_path = []  # Cycle final
        self.actual_vertex = {}  # Sommet sur lequel l'algorithme travail
        self.graph_order = int(collection.count_documents({}))  # Ordre du graphe, récupéré depuis la bdd
        self.visited_matrix = np.zeros(
            (self.graph_order, self.graph_order))  # Matrice qui répertorie les pistes a éliminé

    def back_tracking(self, index_vertex):
        self.initial_vertex = self.collection.find_one({"id_matrix": index_vertex})
        # On ajoute le sommet de départ au chemin de travail
        self.actual_path.append(self.initial_vertex["id_matrix"])
        # On défini le sommet de départ comme étant le sommet de travail
        self.actual_vertex = self.initial_vertex

        # On continue de chercher une solution tant qu'un cycle finale n'a pas été trouvé
        while len(self.saved_path) < self.graph_order:
            # On dresse une liste des voisin disponible du sommet de travail
            available_neighbors = [val for val in self.actual_vertex["neighbors"]
                                   if not self.visited_matrix[self.actual_vertex["id_matrix"]][val["id"]]
                                   and val["id"] != self.initial_vertex["id_matrix"]
                                   and val["id"] not in self.actual_path
                                   or (self.graph_order == len(self.actual_path)
                                       and val["id"] == self.initial_vertex["id_matrix"])]
            # S'il y a au moins un voisin disponible, on l'ajoute au chemin de travail et on le définie comme étant le sommet de travail actuel
            if len(available_neighbors) > 0:
                self.actual_path.append(available_neighbors[0]["id"])
                self.actual_vertex = self.collection.find_one({"id_matrix": available_neighbors[0]["id"]})
            # S'il n'y a pas de voisin disponible
            else:
                # Dans le cas où le cycle serait complet, on le place dans le chemin final
                if len(self.actual_path) == self.graph_order + 1 and self.actual_path[-1] == \
                        self.initial_vertex["id_matrix"]:
                    self.saved_path = self.actual_path
                else:  # Si le cycle n'est pas complet, on retourne en arrière et on retire le sommet de travail actuel du chemin de travail
                    self.visited_matrix[self.actual_path[-2]][self.actual_vertex["id_matrix"]] = 1
                    for i, _ in enumerate(self.visited_matrix[self.actual_vertex["id_matrix"]]):
                        self.visited_matrix[self.actual_vertex["id_matrix"]][i] = 0
                    self.actual_vertex = self.collection.find_one({"id_matrix": self.actual_path[-2]})
                    self.actual_path.pop(-1)
        return self.saved_path



def find_a_neighbor(collection, path):
    check = False
    final_path = []
    counter = 0
    # tant que l'on a pas trouvé de voisin valable on continue de chercher
    while not check:
        counter += 1
        temp_path = path.copy()
        # on choisi deux sommets aléatoire différents dans la solution et qui ne sont pas les sommets d'entré et de sortie
        v1 = random.randint(1, len(temp_path)-2)
        v2 = random.randint(1, len(temp_path)-2)
        while v2 == v1:
            v2 = random.randint(1, len(path)-2)

        # On récupère les informations des sommets dans la base
        v1_obj = collection.find_one({"id_matrix": temp_path[v1]})
        v2_obj = collection.find_one({"id_matrix": temp_path[v2]})

        # on vérifie que les sommets sont bien intervertibles
        v1_neighbors = [val for val in v1_obj["neighbors"]
                        if val["id"] == temp_path[v2 + 1] or val["id"] == temp_path[v2 - 1]]
        v2_neighbors = [val for val in v2_obj["neighbors"]
                        if val["id"] == temp_path[v1 + 1] or val["id"] == temp_path[v1 - 1]]
        if len(v1_neighbors) == 2 and len(v2_neighbors) == 2:
            # on intervertie les deux sommets dans le cycle
            check = True
            final_path = temp_path
            final_path[v1] = v2_obj["id_matrix"]
            final_path[v2] = v1_obj["id_matrix"]
        # Si au bout de 100 essais on ne trouve pas de voisin on arrete de chercher et on renvoie la solution d'origine
        # afin d'éviter les boucles infinies
        # (situation très rare et jamais observé)
        if counter >= 100:
            check = True
    return final_path



def weight(collection, path):
    current_day = 0
    time_global = 0
    for i in range(len(path) - 1):
        actual_vertex = collection.find_one({"id_matrix": path[i]})
        neighbors = actual_vertex["neighbors"]
        neighbor = [val for val in neighbors if val["id"] == path[i + 1]]
        if current_day <= 300 and (current_day + neighbor[0]["traffic_morning"]) < 300:
            current_day = current_day + neighbor[0]["traffic_morning"]
        if current_day <= 420:
            current_day = current_day + neighbor[0]["traffic_afternoon"]
        else:
            time_global = time_global + current_day + 840
            current_day = 0
            if current_day != 0:
                time_global = current_day + time_global
    return time_global / 60


def find_local_optimum(collection, original_path, max_iteration, tabou_list_param):
    path = original_path
    best_weight = weight(collection, original_path)
    counter = 0
    list_neighbor = tabou_list_param.copy()
    # tant que nous ne somme pas au nombre maximum d'itération on cherche des voisins
    while counter < max_iteration:
        counter += 1
        # on cherche un voisin
        temp_path = find_a_neighbor(collection, path)
        # si le voisin n'est pas dans la liste des voisins visité alors on ne calcul le temps de livraison de la solution
        if temp_path not in list_neighbor:
            temp_weight = weight(collection, temp_path)
            if temp_weight < best_weight:  # si le temps est meilleur que l'actuelement enregistré
                if len(tabou_list_param) > 100:
                    tabou_list_param.pop()
                list_neighbor.append(temp_path)
                best_weight = temp_weight  # on remplace le temps precedent par le nouveau
                path = temp_path  # on remplace la solution précédentes par la nouvelle
                counter = 0  # on remet le counter à 0
    return [best_weight, path, list_neighbor]




def meta_tabou_opti(collection, initial_path):
    first_local_optimum = find_local_optimum(collection, initial_path, 15, [])
    best_solution = {
        "path": first_local_optimum[1],
        "weight": first_local_optimum[0]
    }
    tabou_list = [val for val in first_local_optimum[2]]
    iteration = 0
    max_iteration = 25
    no_change_iteration = 0
    max_no_change = 5
    while iteration < max_iteration and no_change_iteration < max_no_change:
        iteration += 1
        neight = find_a_neighbor(collection, best_solution["path"])
        tabou_list.append(neight)
        neight = find_a_neighbor(collection, neight)
        if neight not in tabou_list:
            if len(tabou_list) > 10 * len(neight):
                tabou_list.pop()
            tabou_list.append(neight)
            local_opti = find_local_optimum(collection, neight, 15, tabou_list)
            tabou_list = local_opti[2]
            if best_solution["weight"] > local_opti[0]:
                best_solution["weight"] = local_opti[0]
                best_solution["path"] = local_opti[1]
            else:
                max_no_change += 1
    return [best_solution, tabou_list]