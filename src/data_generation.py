import random
import numpy as np
import pymongo
import time
start = time.time()


def empty_collection(collection):
    collection.delete_many({})


def connect_database():
    client = pymongo.MongoClient('mongodb://localhost:27017/')
    db = client['matrix']
    return db['vertex']


def insert_matrix_in_bdd(collection, matrix):
    quantity_vertex = len(matrix)
    object_row = [{}] * quantity_vertex
    object_matrix = []
    for i in range(quantity_vertex):
        object_matrix.append(object_row.copy())
    for vertex in range(quantity_vertex):
        for vertex2 in range(vertex + 1):
            if matrix[vertex][vertex2]:
                distance = random.randint(5, 30)
                tm = random.randint(30, 50)
                ta = random.randint(30, 50)
                temp_data = {
                    "distance": distance,
                    "traffic_morning": tm,
                    "traffic_afternoon": ta
                }
                object_matrix[vertex][vertex2] = temp_data
                object_matrix[vertex2][vertex] = temp_data
    for vertex in range(len(object_matrix)):
        collection.insert_one({
            "id_matrix": vertex,
            "neighbors": []
        })
    for vertex_id, vertex in enumerate(object_matrix):
        neighbors = []
        for neighbor_id, neighbor in enumerate(vertex):
            if neighbor != {}:
                neighbor["id"] = neighbor_id
                neighbors.append(neighbor)
        collection.update_one({"id_matrix": vertex_id}, {"$set": {"neighbors": neighbors}})


def matrix_generation():
    global start
    # ask for information
    nb_vertices = int(input("enter the number of vertices : "))
    # print(nb_vertices)
    is_density_right = False
    density = 0
    while not is_density_right:
        density = int(input("enter the density between 50% and 100% (graph will not respect exactly the density): "))
        if 50 <= density <= 100:
            is_density_right = True
    start = time.time()
    matrix = np.random.choice((1, 0), size=(nb_vertices, nb_vertices),
                              p=[(density - 10) / 100, 1 - (density - 10) / 100])
    matrix = np.logical_or(matrix, matrix.T)
    for i in range(nb_vertices):
        matrix[i][i] = 0

    # print(matrix)
    # for i in range(nb_vertices):
    #     print(sum(matrix[i]))
    not_valid_row = [row_id for row_id, row in enumerate(matrix) if sum(row) < (nb_vertices / 2)]
    # print(not_valid_row)
    while len(not_valid_row) > 0:
        row_one = random.choice(not_valid_row)
        if len(not_valid_row) < 2:
            choice = [row_id for row_id, val in enumerate(matrix[row_one]) if not val and row_id != row_one]
            row_two = random.choice(choice)
        else:
            temp_not = not_valid_row.copy()
            temp_not.remove(row_one)
            row_two = random.choice(temp_not)
        matrix[row_one][row_two] = 1
        matrix[row_two][row_one] = 1
        if sum(matrix[row_one]) >= nb_vertices / 2:
            not_valid_row.remove(row_one)
        if len(not_valid_row) > 1 and sum(matrix[row_two]) >= nb_vertices / 2:
            not_valid_row.remove(row_two)
    return matrix


def start_generation():
    vertex_collection = connect_database()
    empty_collection(vertex_collection)
    generated_matrix = matrix_generation()
    insert_matrix_in_bdd(vertex_collection, generated_matrix)
    end = time.time()
    print('it take ' + str(end - start) + " seconds")


def count_density():
    collection = connect_database()
    nb_edge = collection.aggregate([{"$group": {"_id": None, "nb_": {"$sum": 1}}}])
    nb_neighbor = collection.aggregate([{"$group": {"_id": None, "nb_": {"$sum": {"$size": "$neighbors"}}}}])
    # density = nb_edge**2 * (nb_neighbor/2)
    print(nb_neighbor.next())


start_generation()
#count_density()

# print(matrix_generation())
