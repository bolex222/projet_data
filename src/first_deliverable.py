from pymongo import MongoClient
from bson.objectid import ObjectId
from pymongo import errors
import datetime
import time

mongo_url = 'mongodb://root:root@localhost:27017'


def connect_to_mongo():
    client = MongoClient(mongo_url)
    try:
        client.server_info()
        print('connexion to mongodb succeed')
        return client
    except errors.ServerSelectionTimeoutError:
        print('connexion to mongodb failed')
        exit()


def get_number_by_ph():
    db = connect_to_mongo()['clients']
    collection = db['traffic']
    results = collection.aggregate([
        {
            '$group': {
                '_id': '$plage_horaire',
                'totalAmount': {'$sum': {'$toInt': '$nb_vehicules'}}
            }
        }
    ])
    for result in results:
        print('total vehicles during period ' + result['_id'] + ': ' + str(result['totalAmount']))


# get_number_by_ph()


def get_time_from_bdd():  # this function convert document's data into datetime format
    # and insert it in document during 13min
    db = connect_to_mongo()['clients']
    collection = db['traffic']
    trafics = collection.find()
    start_time = time.time()
    for trafic in trafics:
        second_date = datetime.datetime(2020, 1, 1) + datetime.timedelta(
            days=int(trafic['num_jour']),
            minutes=int(trafic['num_periode']) + 1,
            hours=7 if trafic['plage_horaire'] == 'm' else 17)  # updating date
        collection.update_one({
            '_id': trafic['_id']
        }, {'$set': {
            'data_stamped': second_date
        }
        })
    print('it take %s s' % (time.time() - start_time))


# get_time_from_bdd()


def insert_stamp_in_new_collection():  # convert data in datetime and insert it in new collection
    db = connect_to_mongo()['clients']
    collection = db['traffic']
    trafics = collection.find()
    data_stamped = []
    for trafic in trafics:
        date = datetime.datetime(2020, 1, 1) + datetime.timedelta(
            days=int(trafic['num_jour']),
            minutes=int(trafic['num_periode']) + 1,
            hours=7 if trafic['plage_horaire'] == 'm' else 17)  # updating date
        data_stamped.append({
            'nb_vehicules': trafic['nb_vehicules'],
            'num_arete': trafic['num_arete'],
            'timestamp': date
        })
    collection_name = 'traffic_' + str(len(db.list_collection_names()) + 1)
    db.create_collection(collection_name)
    db[collection_name].insert_many(data_stamped)


# insert_stamp_in_new_collection()


def remove_data_stamped():  # cleaning function during test
    db = connect_to_mongo()['clients']
    collection = db['traffic']
    collection.update_many({}, {'$unset': {'data_stamped': '', 'data_periode': ''}})


# remove_data_stamped()


def count_document_new_collection():  # count last collection length
    db = connect_to_mongo()['clients']
    collection = db[db.list_collection_names()[len(db.list_collection_names()) - 1]]
    print('number of documents in collection ' + str(db.list_collection_names()[len(db.list_collection_names()) - 1]) +
          ': ' + str(collection.count_documents({})))


# count_document_new_collection()


def show_all_datetime():  # show all document's datetime
    db = connect_to_mongo()['clients']
    collection = db[db.list_collection_names()[len(db.list_collection_names()) - 1]]
    for document in collection.find():
        print(document['timestamp'])


# show_all_datetime()


def show_nb_vehicles_by_schedule():
    db = connect_to_mongo()['clients']
    collection = db[db.list_collection_names()[len(db.list_collection_names()) - 1]]
    results = collection.aggregate([
        {
            '$group': {
                '_id': {
                    '$cond': {
                        'if': {'$gte': [{'$hour': '$timestamp'}, 12]}, 'then': 'pm', 'else': 'am'
                    }
                },
                'totalAmount': {'$sum': {'$toInt': '$nb_vehicules'}}
            }
        }
    ])
    for result in results:
        print('total vehicles during period ' + str(result['_id']) + ': ' + str(result['totalAmount']))


# show_nb_vehicles_by_schedule()
