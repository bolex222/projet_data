import math
import random
import time

import pymongo
global_path = [0, 1, 2, 4, 3, 5, 7, 9, 10, 6, 8, 12, 13, 11, 15, 14, 17, 16, 18, 20, 21, 22, 19, 23, 24, 25, 27, 26, 28, 29, 30, 32, 31, 33, 35, 36, 34, 37, 38, 39, 40, 41, 44, 43, 46, 42, 45, 49, 47, 50, 48, 51, 52, 53, 54, 55, 56, 58, 57, 59, 60, 62, 63, 61, 65, 64, 67, 69, 68, 70, 71, 74, 66, 77, 72, 73, 75, 76, 79, 83, 78, 80, 81, 82, 85, 84, 87, 86, 88, 89, 91, 93, 90, 92, 95, 94, 96, 97, 98, 99, 0]


def connect_database():
    client = pymongo.MongoClient('mongodb://localhost:27017/')
    db = client['matrix']
    return db['vertex']


def find_a_neighbor(collection, path):
    check = False
    final_path = []
    counter = 0
    while not check:
        counter += 1
        temp_path = path.copy()
        v1 = random.randint(1, len(path)-2)
        v2 = random.randint(1, len(path)-2)
        while v2 == v1:
            v2 = random.randint(1, len(path)-2)

        v1_obj = collection.find_one({"id_matrix": temp_path[v1]})
        v2_obj = collection.find_one({"id_matrix": temp_path[v2]})

        v1_neighbors = [val for val in v1_obj["neighbors"]
                        if val["id"] == temp_path[v2 + 1] or val["id"] == temp_path[v2 - 1]]
        v2_neighbors = [val for val in v2_obj["neighbors"]
                        if val["id"] == temp_path[v1 + 1] or val["id"] == temp_path[v1 - 1]]

        if len(v1_neighbors) == 2 and len(v2_neighbors) == 2:
            # print(v1_obj["id_matrix"], v1_neighbors)
            # print(v2_obj["id_matrix"], v2_neighbors)
            check = True
            final_path = temp_path
            final_path[v1] = v2_obj["id_matrix"]
            final_path[v2] = v1_obj["id_matrix"]
        if counter >= 100:
            check = True
    return final_path


def weight(collection, path):
    current_day = 0
    time_global = 0
    for i in range(len(path) - 1):
        actual_vertex = collection.find_one({"id_matrix": path[i]})
        neighbors = actual_vertex["neighbors"]
        neighbor = [val for val in neighbors if val["id"] == path[i + 1]]
        if current_day <= 300 and (current_day + neighbor[0]["traffic_morning"]) < 300:
            current_day = current_day + neighbor[0]["traffic_morning"]
        if current_day <= 420:
            current_day = current_day + neighbor[0]["traffic_afternoon"]
        else:
            time_global = time_global + current_day + 840
            current_day = 0
            if current_day != 0:
                time_global = current_day + time_global
    return time_global / 60


def find_local_optimum(original_path, max_iteration, tabou_list_param):
    path = original_path
    collection = connect_database()
    best_weight = weight(collection, original_path)
    counter = 0
    list_neighbor = tabou_list_param.copy()
    while counter < max_iteration:
        counter += 1
        if counter % 5 == 0:
            print("deg 1 : " + str(counter))
        temp_path = find_a_neighbor(collection, path)
        if temp_path not in list_neighbor:
            temp_weight = weight(collection, temp_path)
            if temp_weight < best_weight:
                if len(tabou_list_param) > 100:
                    tabou_list_param.pop()
                list_neighbor.append(temp_path)
                print("nouvelle meilleur solution" + str(temp_weight))
                best_weight = temp_weight
                path = temp_path
                counter = 0
    return [best_weight, path, list_neighbor]


# print(global_path)
# start = time.time()
# print(weight(connect_database(), global_path))
# result = find_local_optimum(global_path, int(input("itération max : ")))
# end = time.time()
# print("temps de résolution en seconde : " + str(end - start) + "s")
# print(result[1])
# print(result[0])


def meta_tabou_opti(collection, initial_path):
    first_local_optimum = find_local_optimum(initial_path, 25, [])
    best_solution = {
        "path": first_local_optimum[1],
        "weight": first_local_optimum[0]
    }
    tabou_list = [val for val in first_local_optimum[2]]
    iteration = 0
    max_iteration = 25
    no_change_iteration = 0
    max_no_change = 5
    while iteration < max_iteration and no_change_iteration < max_no_change:
        iteration += 1
        print("deg 2 : " + str(iteration))
        neight = find_a_neighbor(collection, best_solution["path"])
        tabou_list.append(neight)
        neight = find_a_neighbor(collection, neight)
        if neight not in tabou_list:
            if len(tabou_list) > 10 * len(neight):
                tabou_list.pop()
            tabou_list.append(neight)
            local_opti = find_local_optimum(neight, 25, tabou_list)
            tabou_list = local_opti[2]
            if best_solution["weight"] > local_opti[0]:
                best_solution["weight"] = local_opti[0]
                best_solution["path"] = local_opti[1]
                print("best soluce deg 2 : " + str(best_solution["weight"]))
            else:
                max_no_change += 1
    print(no_change_iteration, iteration)
    return [best_solution, tabou_list]


print(global_path)
print(weight(connect_database(), global_path))
start = time.time()
result = meta_tabou_opti(connect_database(), global_path)
print(result[0])
print(result[1])
