# Livrable 2 Groupe 7

## L'objectif :
Nous allons devoir créer un algorithme permettant de calculer sur un réseau routier une tournée qui relie un ensemble de points puis de revenir à son point de départ de manière à minimiser la durée totale de la tournée en fonction du nombre de véhicules.

## Les contraintes
- Le livreur ne doit pas travailler plus de 7 heures/jour défini par une plage horaire. 
- Une pause de 5 min toutes les 2 heures afin d’éviter les risques d’accidents.
- Le livreur devra respecter le trafic.

## La solution
Pour résoudre ce problème, nous allons utiliser une solution liée au problème de voyageur de commerce auquel nous allons ajouter nos contraintes :
- pour respecter le trafic, le poids de chaque arête sera calculé à partir de la distance et du trafic pour donner un temps.
- pour les pauses et le temps de travail par jour, nous compterons le temps de travail à partir du poids de chaque arêtes.


## Problématique sous forme mathématique

Nous cherchons ensuite à traduire mathématiquement l'étude de la tournée de livraison. Dans un premier temps, nous définirons les différentes variables du problème puis nous expliquerons à l'aide d'équations les contraintes imposées. 

### Variables
- On a tout d'abord le Graphe hamiltonien et connexe G=(V,E).
- V étant l’ensemble des sommets.
- E étant l’ensemble des arêtes, chacune représentée par un temps *t* en fonction d'une distance D en km et un Trafic T en km/h (t = D/T).
- On a le nombre de Livreur N (constante).
- On a le temps de pause p en minutes et qui devra s’exécuter toutes les H heures (toutes deux des constantes).
- On a également le temps de travail maximum par jour Wt en heures (constante).
- On a finalement le temps de travail total TWT en jours:heures:minutes.

## Formulation
On déduira donc TWT grâce au problème de voyageur de commerce basé sur le graphe G dont on calculera *t* pour chaque arête basée sur T et D. 
Le voyageur ne pourras pas rouler plus de Wt heures par jour et plus de H heures d'affilée sans faire de pause de *p* minutes.


## Complexité

D'après nos recherches le problème de voyageur de commerce étant de complexité NP-Difficile. Si on ramène notre problème à celui de voyageur de commerce auquel on ajoute nos contraintes, la complexité de notre problème en est donc au moins aussi difficile et rentre donc dans la classe NP-Difficile.

Quelques ressources :

[Routific](https://blog.routific.com/travelling-salesman-problem)
[tutorialpoint](https://www.tutorialspoint.com/design_and_analysis_of_algorithms/design_and_analysis_of_algorithms_np_hard_complete_classes.htm)
[datavis](https://discord.com/channels/848869642893393930/852919991266836540/854007320681906206)
[Interstice](https://interstices.info/le-probleme-du-voyageur-de-commerce/)
[GeeksforGeeks](https://www.geeksforgeeks.org/proof-that-traveling-salesman-problem-is-np-hard/)
