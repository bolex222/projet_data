# Data Project CESI 2021

This is group 7 repository's for data project at CESI.

You will find the mongo dB environment for docker and the python 🐍 code.

## Installation

We are using [docker](https://docs.docker.com/get-docker/) to run this project.

## Usage
Pull this repository.

Use docker-compose to run our mongoDB container.

```bash
docker-compose -d up
```

## Configuration

Database credentials are located in docker-compose.yml


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. 

[gitlab link](https://gitlab.com/bolex222/projet_data)

## Credits
[Basile LECOUTURIER](https://www.linkedin.com/in/blecout/) - Jérémy CROSBY - Gaultier GELLEN - Martin Cabaud - Antoine LABAT

